package com.example.weatherapp;

public class Forecast {
    String temp;
    String time;
    int icon;

    public Forecast(String ptemp, String ptime, int picon)
    {
        temp = ptemp;
        time = ptime;
        icon = picon;
    }
}
