package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ForecastActivity extends AppCompatActivity {

    private static final String TAG = "all";
    int count = 0;
    String city;
    JSONObject forecast;
    RequestQueue requestQueue;
    ArrayList<Forecast> forecastArrayList;

    ForecastRecyclerViewAdapter adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        Intent intent = getIntent();
        city = intent.getStringExtra("City");

        recyclerView = findViewById(R.id.forecast_recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        forecastArrayList = new ArrayList<>();
        adapter = new ForecastRecyclerViewAdapter(this, forecastArrayList);
        recyclerView.setAdapter(adapter);


        requestQueue = Volley.newRequestQueue(this);
        getWeather();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG);
        }
    }

    private void getWeather()
    {
        String givenCity = city;
        String url = String.format("https://api.openweathermap.org/data/2.5/forecast?q=%s&appid=670c3aa6c07aaff1a2ad122818c5d0dd&units=metric", givenCity);

        // Pass second argument as "null" for GET requests
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            forecast = response;
                            count = forecast.getInt("cnt");
                            setWeather();
                        } catch (Exception e) {
                            Log.e("Error", "Weather request error");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error response: ", error.getMessage());
            }
        });

        /* Add your Requests to the RequestQueue to execute */
        req.setTag(TAG);
        requestQueue.add(req);
    }

    private void setWeather() {

        try {
            for (int i = 0; i < count; i++) {
                String icon = "icon_" + forecast.getJSONArray("list").getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon");
                String time = forecast.getJSONArray("list").getJSONObject(i).getString("dt_txt");
                String temp2 = forecast.getJSONArray("list").getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("description");
                String temp3 = forecast.getJSONArray("list").getJSONObject(i).getJSONObject("main").getString("temp");
                String whole_temp = temp3 + " °C | " + temp2;
                int resourceId = getResources().getIdentifier(icon, "drawable", getPackageName());
                Forecast tempForecast = new Forecast(whole_temp, time, resourceId);
                forecastArrayList.add(tempForecast);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter = new ForecastRecyclerViewAdapter(this, forecastArrayList);
        recyclerView.setAdapter(adapter);

    }

}