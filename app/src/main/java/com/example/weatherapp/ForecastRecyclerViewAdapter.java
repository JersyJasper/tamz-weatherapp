package com.example.weatherapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class ForecastRecyclerViewAdapter extends RecyclerView.Adapter<ForecastRecyclerViewAdapter.ViewHolder>{

    private List<Forecast> mData;
    private LayoutInflater layoutInflater;
    private ItemClickListener onItemClickListener;

    // data is passed into the constructor
    public ForecastRecyclerViewAdapter(Context context, List<Forecast> data) {
        this.layoutInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ForecastRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowItem = layoutInflater.inflate(R.layout.forecast_row, parent, false);
        return new ViewHolder(rowItem);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ForecastRecyclerViewAdapter.ViewHolder holder, int position) {
        Forecast forecast = mData.get(position);
        holder.temp.setText(forecast.temp);
        holder.time.setText(String.valueOf(forecast.time));
        holder.icon.setImageResource(forecast.icon);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView temp;
        private TextView time;
        private ImageView icon;


        public ViewHolder(View itemView) {
            super(itemView);
            this.temp = itemView.findViewById(R.id.forecast_temp);
            this.time = itemView.findViewById(R.id.forecast_time);
            this.icon = itemView.findViewById(R.id.forecast_icon);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null)
            {
                onItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id).time;
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.onItemClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
