package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    //TODO: Description and celsium and forecast
    private static final String TAG = "all";
    TextView temperature_textView;
    TextView description_textView;
    ImageView icon_imageView;
    EditText city;
    JSONObject current_weather;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        temperature_textView = findViewById(R.id.temperature_textView);
        description_textView = findViewById(R.id.description_textView);
        icon_imageView = findViewById(R.id.icon_imageView);
        city = findViewById(R.id.city_editText);
        requestQueue = Volley.newRequestQueue(this);

        city.setText(R.string.defaultCity);
        if (isNetworkConnected())
        {
            getWeather();
        }
        else
        {
            internetError();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG);
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        Network activeNetwork = connectivityManager.getActiveNetwork();

        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);

        return networkCapabilities != null &&
                networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
    }

    private void getWeather()
    {
        if (city.getText().toString().isEmpty())
        {

            return;
        }
        String givenCity = city.getText().toString();
        String url = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=670c3aa6c07aaff1a2ad122818c5d0dd&units=metric", givenCity);

        // Pass second argument as "null" for GET requests
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            current_weather = response;
                            setWeather();
                        } catch (Exception e) {
                            Log.e("Error", "Weather request error");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error response: ", error.getMessage());
            }
        });

        /* Add your Requests to the RequestQueue to execute */
        req.setTag(TAG);
        requestQueue.add(req);
    }

    private void setWeather() {

        try {
            String icon = "icon_" + current_weather.getJSONArray("weather").getJSONObject(0).getString("icon");
            int resourceId = getResources().getIdentifier(icon, "drawable", getPackageName());
            icon_imageView.setImageResource(resourceId);

            String main = current_weather.getJSONArray("weather").getJSONObject(0).getString("main");
            String description = current_weather.getJSONArray("weather").getJSONObject(0).getString("description");
            String whole_description = main + "\n" + description;
            description_textView.setText(whole_description);

            String temp = String.format("%s °C", current_weather.getJSONObject("main").getString("temp"));
            String temp_min = String.format("%s °C", current_weather.getJSONObject("main").getString("temp_min"));
            String temp_max = String.format("%s °C", current_weather.getJSONObject("main").getString("temp_max"));
            String whole_temperatures = temp + "\nMin: " + temp_min + ", Max: " + temp_max;
            temperature_textView.setText(whole_temperatures);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void ok_click(View view) {
        if (isNetworkConnected())
        {
            getWeather();
        }
        else
        {
            internetError();
        }
    }
    private void internetError() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("Please check your internet connection and try again");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void openForecast(View view) {
        String givenCity = city.getText().toString();
        Intent intent = new Intent(this, ForecastActivity.class);
        intent.putExtra("City", givenCity);
        startActivity(intent);

    }
}